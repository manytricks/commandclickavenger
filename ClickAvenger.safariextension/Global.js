// Copyright © 2014-2016 Many Tricks (When in doubt, consider this MIT-licensed)


var theApplication = safari.application;

theApplication.addEventListener('message', function (theEvent) {
	if (theEvent.name=='com.manytricks.ClickAvenger.RequestSettings') {
		var theSettings = safari.extension.settings;
		theEvent.target.page.dispatchMessage('com.manytricks.ClickAvenger.DeliverSettings', {
			'AutoAvenge.Scope': theSettings.getItem('com.manytricks.ClickAvenger.AutoAvenge.Scope'),
			'AutoAvenge.Whitelist': theSettings.getItem('com.manytricks.ClickAvenger.AutoAvenge.Whitelist')
		});
	}
}, false);

theApplication.addEventListener('contextmenu', function (theEvent) {
	var theURL = theEvent.userInfo;
	if ((theURL) && (theURL!=='')) {
		var theTitle;
		switch (navigator.language) {
			case 'de-at':
			case 'de-ch':
			case 'de-de':
				theTitle = 'Link im aktuellen Tab öffnen';
				break;
			default:
				theTitle = 'Open Link in Current Tab';
				break;
		}
		theEvent.contextMenu.appendContextMenuItem('com.manytricks.ClickAvenger.Avenge', theTitle, null);
	}
}, false);

theApplication.addEventListener('command', function (theEvent) {
	if (theEvent.command=='com.manytricks.ClickAvenger.Avenge') {
		theApplication.activeBrowserWindow.activeTab.url = theEvent.userInfo;
	}
}, false);
